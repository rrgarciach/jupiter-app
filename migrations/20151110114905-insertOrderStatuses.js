(function () {
  'use strict';

  var q = require('q');
  var orderStatuses = [
    {name: 'captured', description: 'Capturado'},
    {name: 'processed', description: 'Procesado'},
    {name: 'working', description: 'Surtiendo'},
    {name: 'stocked', description: 'Surtido'},
    {name: 'pending_delivery', description: 'Pendiente de Entrega'},
    {name: 'on_delivery', description: 'En Ruta para Entrega'},
    {name: 'delivered', description: 'Entregado'},
    {name: 'canceled', description: 'Cancelado'},
    {name: 'canceled_by_client', description: 'Cancelado por el Cliente'},
    {name: 'canceled_by_promoter', description: 'Cancelado por el Promotor'},
  ];
  var orderDetailStatuses = [
    {name: 'captured', description: 'Capturado'},
    {name: 'processed', description: 'Procesado'},
    {name: 'working', description: 'Surtiendo'},
    {name: 'stocked', description: 'Surtido'},
    {name: 'pending_delivery', description: 'Pendiente de Entrega'},
    {name: 'on_delivery', description: 'En Ruta para Entrega'},
    {name: 'delivered', description: 'Entregado'},
    {name: 'canceled', description: 'Cancelado'},
    {name: 'canceled_by_client', description: 'Cancelado por el Cliente'},
    {name: 'canceled_by_promoter', description: 'Cancelado por el Promotor'},
  ];

  module.exports = {
    up: function (queryInterface) {
      var insertOrderStatusesPromise = function (status) {
        return queryInterface.sequelize.query(
          'INSERT INTO `OrderStatuses` (`id`, `name`, `description`) VALUES (NULL, "' + status.name + '", "' + status.description + '")',
          {type: queryInterface.sequelize.QueryTypes.INSERT}
        );
      };

      var insertOrderDetailStatusesPromise = function (status) {
        return queryInterface.sequelize.query(
          'INSERT INTO `OrderDetailStatuses` (`id`, `name`, `description`) VALUES (NULL, "' + status.name + '", "' + status.description + '")',
          {type: queryInterface.sequelize.QueryTypes.INSERT}
        );
      };

      var getPromises = function () {
        var retPromises = [];

        console.log('Inserting statuses in table OrderStatuses...');
        orderStatuses.forEach(function (statusName) {
          retPromises.push(insertOrderStatusesPromise(statusName));
        });

        console.log('Inserting statuses in table OrderDetailStatuses...');
        orderDetailStatuses.forEach(function (statusName) {
          retPromises.push(insertOrderDetailStatusesPromise(statusName));
        });

        return retPromises;
      };

      return q.all(getPromises())
    },

    down: function (queryInterface) {
      var deleteOrderStatusesPromise = function (statusName) {
        return queryInterface.sequelize.query(
          'DELETE FROM `OrderStatuses` WHERE `name` = "' + statusName + '"',
          {type: queryInterface.sequelize.QueryTypes.DELETE}
        );
      };

      var deleteOrderDetailStatusesPromise = function (statusName) {
        return queryInterface.sequelize.query(
          'DELETE FROM `OrderDetailStatuses` WHERE `name` = "' + statusName + '"',
          {type: queryInterface.sequelize.QueryTypes.DELETE}
        );
      };

      var getPromises = function () {
        var retPromises = [];

        console.log('Removing statuses from table OrderDetailStatuses...');
        orderDetailStatuses.forEach(function (statusName) {
          retPromises.push(deleteOrderDetailStatusesPromise(statusName));
        });

        console.log('Removing statuses from table OrderStatuses...');
        orderStatuses.forEach(function (statusName) {
          retPromises.push(deleteOrderStatusesPromise(statusName));
        });
        return retPromises;
      };

    }
  }
})();