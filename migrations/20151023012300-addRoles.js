(function () {
  'use strict';

  module.exports = {
    up: function (queryInterface, Sequelize) {
      console.log('Creating Roles table...');
      return queryInterface.createTable('Roles',
        {
          id: {
            type: Sequelize.INTEGER,
            unique: true,
            primaryKey: true,
            autoIncrement: true
          },
          name: {
            type: Sequelize.STRING
          }
        }, {
          timestamps: true,
          paranoid: false,
          charset: 'utf8' // default: null
        }
      );
    },

    down: function (queryInterface) {
      console.log('Removing Roles table...');
      return queryInterface.dropTable('Roles');

    }
  }
})();