(function () {
  'use strict';

  module.exports = {
    up: function (queryInterface, Sequelize) {
      console.log('Creating Addresses table...');
      return queryInterface.createTable('Addresses',
        {
          id: {
            type: Sequelize.INTEGER,
            unique: true,
            primaryKey: true,
            autoIncrement: true,
            validate: {
              isInt: true
            }
          },
          street: {
            type: Sequelize.STRING,
            allowNull: false
          },
          number: {
            type: Sequelize.INTEGER,
            validate: {
              isInt: true
            }
          },
          interior: {
            type: Sequelize.STRING
          },
          suburb: {
            type: Sequelize.STRING,
            allowNull: false
          },
          zip: {
            type: Sequelize.INTEGER,
            validate: {
              isInt: true
            }
          },
          city: {
            type: Sequelize.STRING,
            allowNull: false
          },
          county: {
            type: Sequelize.STRING,
            allowNull: false
          },
          state: {
            type: Sequelize.STRING,
            allowNull: false
          },
          phoneId1: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
              model: 'Phones',
              key: 'id'
            }
          },
          phoneId2: {
            type: Sequelize.INTEGER,
            references: {
              model: 'Phones',
              key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'SET NULL'
          },
          faxId: {
            type: Sequelize.INTEGER,
            references: {
              model: 'Phones',
              key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'SET NULL'
          },
          latitude: {
            type: Sequelize.FLOAT,
            allowNull: false
          },
          longitude: {
            type: Sequelize.FLOAT,
            allowNull: false
          }
        }, {
          timestamps: true,
          paranoid: false,
          charset: 'utf8' // default: null
        }
      );
    },

    down: function (queryInterface) {
      console.log('Removing Addresses table...');
      return queryInterface.dropTable('Addresses');

    }
  }
})();