(function () {
  'use strict';

  module.exports = {
    up: function (queryInterface, Sequelize) {
      console.log('Creating Phones table...');
      return queryInterface.createTable('Phones',
        {
          id: {
            type: Sequelize.INTEGER,
            unique: true,
            primaryKey: true,
            autoIncrement: true,
            validate: {
              isInt: true
            }
          },
          number: {
            type: Sequelize.STRING,
            allowNull: false
          },
          type: {
            type: Sequelize.ENUM,
            values: ['Phone', 'Fax', 'Mobile']
          }
        }, {
          timestamps: true,
          paranoid: false,
          charset: 'utf8' // default: null
        }
      );
    },

    down: function (queryInterface) {
      console.log('Removing Phones table...');
      return queryInterface.dropTable('Phones');

    }
  }
})();