(function () {
  'use strict';

  module.exports = {
    up: function (queryInterface, Sequelize) {
      console.log('Creating Promoters table...');
      return queryInterface.createTable('Promoters',
        {
          id: {
            type: Sequelize.INTEGER,
            unique: true,
            primaryKey: true,
            autoIncrement: true,
            validate: {
              isInt: true
            }
          },
          userId: {
            type: Sequelize.INTEGER,
            allowNull: false,
            validate: {
              isInt: true
            },
            references: {
              model: 'Users',
              key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
          },
          addressId: {
            type: Sequelize.STRING,
            allowNull: false,
            validate: {
              isInt: true
            }
          }
        }, {
          timestamps: true,
          paranoid: false,
          charset: 'utf8' // default: null
        }
      );
        //.then(function () {
        //  return queryInterface.changeColumn('Orders', 'promoter', {
        //    type: Sequelize.INTEGER,
        //    references: {
        //      model: 'Promoters',
        //      key: 'id',
        //      referenceKey: 'id'
        //    },
        //    onUpdate: 'CASCADE',
        //    onDelete: 'SET NULL'
        //  })
        //    .then(function () {
        //      return queryInterface.renameColumn('Orders', 'promoter', 'PromoterId');
        //    });
        //});
    },

    down: function (queryInterface) {
      console.log('Removing Promoters table...');
      return queryInterface.dropTable('Promoters');

    }
  }
})();