(function () {
  'use strict';

  var q = require('q');

  module.exports = {
    up: function (queryInterface) {
      console.log('Inserting default Phones...');
      return queryInterface.sequelize.query(
        'INSERT INTO `Phones` (`id`, `number`, `type`) VALUES (1, "6144824200", "Phone"), (2, "6144194536", "Fax"), (3, "6141845418", "Mobile")',
        {type: queryInterface.sequelize.QueryTypes.INSERT}
      )

        .then(function () {
          console.log('Inserting default Address...');
          return queryInterface.sequelize.query(
            'INSERT INTO `Addresses` (`id`, `street`, `number`, `interior`, `suburb`, `zip`, `city`, `county`, `state`, `phoneId1`, `phoneId2`, `faxId`, `latitude`, `longitude`) ' +
            'VALUES (1, "Cossio", 4310, "B", "Las granjas", "31100", "Chihuahua", "Chihuahua", "Chihuahua", 1, NULL, NULL, 28.662613, -106.102897)',
            {type: queryInterface.sequelize.QueryTypes.INSERT}
          )
        })

        .then(function () {
          console.log('Inserting default Promoter...');
          return queryInterface.sequelize.query(
            'INSERT INTO `Promoters` (`id`, `UserId`, `AddressId`) VALUES (1, 1, 1)',
            {type: queryInterface.sequelize.QueryTypes.INSERT}
          );
        });
    },

    down: function (queryInterface) {
      console.log('Removing Default Promoter...');
      return queryInterface.sequelize.query(
        'DELETE FROM `Promoters` WHERE `id` = 1',
        {type: queryInterface.sequelize.QueryTypes.DELETE}
      )
        .then(function () {
          return queryInterface.sequelize.query(
            'DELETE FROM `Addresses` WHERE `id` = 1',
            {type: queryInterface.sequelize.QueryTypes.DELETE}
          )
        })

        .then(function () {
          return queryInterface.sequelize.query(
            'DELETE FROM `Phones` WHERE `id` IN (1, 2, 3)',
            {type: queryInterface.sequelize.QueryTypes.DELETE}
          )
        });
    }

  }

})();