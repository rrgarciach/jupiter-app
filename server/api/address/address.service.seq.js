(function () {
  'use strict';

  const models = require('../../models');
  const Address = models.Address;
  const Phone = models.Phone;
  const User = models.User;
  const Promoter = models.Promoter;
  //const Client = models.Client;
  const q = require('q');
  const jsonErrorFactory = require('../../components/json_error');

  /**
   * Get all Addresses from given User ID
   * @param User
   * @returns {deferred.promise|{then, always}}
   */
  function getAddressesByUserId(User) {
    let deferred = q.defer();

    let Subject = {};
    if (User.Role.name === 'Client') {
      //Subject = Client;
    } else {
      Subject = Promoter;
    }

    Subject.findOne({
      where: { userId: User.id },
      include: [{
        model: Address,
        as: 'Address', // @TODO: Make sure that this works for Addresses in Client.
        include: [{
          all: true,
          nested: true,
          attributes: ['id', 'number', 'type']
        }]
      }]
    })
      .then(user => {
        if (!user) {
          let jsonError = jsonErrorFactory(401, 'Unauthorized', 'Current User is not a Promoter');
          deferred.reject(jsonError);
          return deferred.promise;
        }
        deferred.resolve(user.Address);
      })
      .catch(err => {
        let jsonError = jsonErrorFactory(500, 'Internal Error', err);
        deferred.reject(jsonError);
      });

    return deferred.promise;
  }

  /**
   * Get single Address by ID
   * @param addressId
   * @param userId
   * @returns {deferred.promise|{then, always}}
   */
  function getAddressById(addressId, userId) {
    let deferred = q.defer();
    let validation = _validateUser(addressId, userId);

    if (validation) {
      Address.findById(addressId, {
        include: [{
          all: true,
          nested: true,
          attributes: ['id', 'number', 'type']
        }]
      })
        .then(address => {
          deferred.resolve(address);
        })
        .catch(err => {
          let jsonError = jsonErrorFactory(500, 'Internal Error', err);
          deferred.reject(jsonError);
        });
    } else {
      let jsonError = jsonErrorFactory(401, 'Unauthorized', 'Address does not belong to current User');
      deferred.reject(jsonError);
    }

    return deferred.promise;
  }

  /**
   * Create a Address
   * @param newAddressData
   * @returns {deferred.promise|{then, always}}
   */
  function createAddress(newAddressData) {
    let deferred = q.defer();
    if (newAddressData.id) { delete newAddressData.id; } // Remove ID to avoid modifying it

    // Build and save Phones
    let promisesForAddress = _savePhones(newAddressData);
    q.all(promisesForAddress)
      .then((phones) => {
        newAddressData.phoneId1 = phones[0].dataValues.id;
        if (phones[1]) { newAddressData.phoneId2 = phones[1].dataValues.id; }
        if (phones[2]) { newAddressData.faxId = phones[2].dataValues.id; }

        // Build and save Address
        let newAddressInstance = Address.build(newAddressData);
        newAddressInstance.save()
          .then(newAddress => {
            deferred.resolve(newAddress);
          })
          .catch(err => { // Catch errors trying to save Address
            if (err.message.indexOf('Validation error') > -1) { // indexOf because message is sent as a string
              let jsonError = jsonErrorFactory(400, 'Bad Request', err.message);
              deferred.reject(jsonError);
              throw new Error('Bad Request'); // got to catch this way because validations are not async =(

            } else {
              let jsonError = jsonErrorFactory(500, 'Internal Error', err);
              deferred.reject(jsonError);

            }
            return deferred.promise;
          });
      })
      .catch(err => {
        let jsonError = jsonErrorFactory(500, 'Internal Error', err);
        deferred.reject(jsonError);
      });

    return deferred.promise;
  }

  /**
   * Update a Address by ID
   * @param addressId
   * @param newData
   * @param userId
   * @returns {deferred.promise|{then, always}}
   */
  function editAddress(addressId, newData, userId) {
    let deferred = q.defer();
    let validation = _validateUser(addressId, userId); // Validate if Address belongs to current User

    if (validation) {
      // Check that provided Address ID exists:
      Address.findOne({
        where: {id: addressId},
      })
        .then(address => {
          if (newData.id) { delete newData.id; } // Remove ID to avoid modifying it

          // If Address is not found:
          if (!address) {
            let jsonError = jsonErrorFactory(404, 'Not Found', 'Provided Address ID not found');
            deferred.reject(jsonError);

          } else { // If Address exists:
            if (newData.id) { delete newData.id; } // Remove ID to avoid modifying it

            // Build and save Phones
            let promisesForAddress = _updatePhones(address, newData);
            q.all(promisesForAddress)
              .then((phones) => {
                newData.phoneId1 = phones[0].dataValues.id;
                if (phones[1]) { newData.phoneId2 = phones[1].dataValues.id; }
                if (phones[2]) { newData.faxId = phones[2].dataValues.id; }

                address.update(newData)
                  .then(updatedAddress => {
                    deferred.resolve(updatedAddress);
                  })
                  .catch(err => { // Catch validation Error throws:
                    if (err.message.indexOf('Validation error') > -1) { // indexOf because message is sent as a string
                      let jsonError = jsonErrorFactory(400, 'Bad Request', err.message);
                      deferred.reject(jsonError);
                      throw new Error('Bad Request'); // got to catch this way because validations are not async =(

                    } else {
                      let jsonError = jsonErrorFactory(500, 'Internal Error', err);
                      deferred.reject(jsonError);

                    }
                    return deferred.promise;
                  });
              })
            .catch(err => { // Catch errors for Phones update
                  let jsonError = jsonErrorFactory(400, 'Bad Request', err);
                  deferred.reject(jsonError);
              });
          }
        })
        .catch(err => { // Catch errors for Address search
          let jsonError = jsonErrorFactory(500, 'Internal Error', err);
          deferred.reject(jsonError);
        });

    } else { // If validation fails due that Address does not belongs to provided User
      let jsonError = jsonErrorFactory(401, 'Unauthorized', 'Address does not belong to current User');
      deferred.reject(jsonError);
    }

    return deferred.promise;
  }

  /**
   * Delete and Address by ID
   * @param addressId
   * @param userId
   * @returns {deferred.promise|{then, always}}
   */
  function deleteAddress(addressId, userId) {
    let deferred = q.defer();
    let validation = _validateUser(addressId, userId);

    if (validation) {
      Address.findOne({
        where: {id: addressId},
        attributes: ['id', 'phoneId1', 'phoneId2', 'faxId'],
      })
        .then(address => {
          if (!address) {
            let jsonError = jsonErrorFactory(404, 'Not Found', 'Provided Address ID not found');
            deferred.reject(jsonError);
          } else {
            address.destroy()
              .then(function () {
                _deletePhone(address.dataValues.phoneId1);
                _deletePhone(address.dataValues.phoneId2);
                _deletePhone(address.dataValues.faxId);
                deferred.resolve(true);
              }, function (err) {
                let jsonError = jsonErrorFactory(500, 'Internal Error', err);
                deferred.reject(jsonError);
                return deferred.promise;
              });
          }
        }, err => {
          let jsonError = jsonErrorFactory(500, 'Internal Error', err);
          deferred.reject(jsonError);
        });
    } else {
      let jsonError = jsonErrorFactory(401, 'Unauthorized', 'Address does not belong to current User');
      deferred.reject(jsonError);
    }

    return deferred.promise;
  }

  /**
   * Builds and saves Phones Instances
   * @param newAddressData
   * @returns {*}
   * @private
   */
  function _savePhones(newAddressData) {
    let promisesForAddress = []; // Array of promises
    // Build and save Phone 1
    if (newAddressData.Phone1) {
      let promisePhone1 = _savePhone(newAddressData.Phone1);
        //.then(phone1 => {
        //  newAddressData.phoneId1 = phone1.id;
        //});
      promisesForAddress.push(promisePhone1);
    } else {
      let deferred = q.defer();
      deferred.reject('Phone 1 data was not provided');
      promisesForAddress.push(deferred.promise);
    }

    // Build and save Phone 2
    if (newAddressData.Phone2) {
      let promisePhone2 = _savePhone(newAddressData.Phone2);
        //.then(phone2 => {
        //  newAddressData.phoneId2 = phone2.id;
        //});
      promisesForAddress.push(promisePhone2);
    }

    // Build and save Fax
    if (newAddressData.Fax) {
      let promiseFax = _savePhone(newAddressData.Fax);
        //.then(fax => {
        //  newAddressData.faxId = fax.id;
        //});
      promisesForAddress.push(promiseFax);
    }

    // Resolve al references builds
    return promisesForAddress;
  }

  /**
   * Builds and saves Phone Instance
   * @param newPhoneData
   * @returns {Promise.<this|Errors.ValidationError>}
   * @private
   */
  function _savePhone(newPhoneData) {
    let newPhoneInstance = Phone.build(newPhoneData);
    return newPhoneInstance.save()
  }

  /**
   * Builds and updates Phones Instances
   * @param newData
   * @returns {*}
   * @private
   */
  function _updatePhones(Address, newData) {
    let promisesForAddress = []; // Array of promises

    // Build and save Phone 1
    if (newData.Phone1) {
      let promisePhone1 = _updatePhone(Address.dataValues.phoneId1, newData.Phone1);
      promisesForAddress.push(promisePhone1);

    } else {
      let deferred = q.defer();
      deferred.reject('Phone 1 data was not provided');
      promisesForAddress.push(deferred.promise);
    }

    // Build and save Phone 2
    if (newData.Phone2) {
      let promisePhone2 = {};

      if (!Address.dataValues.phoneId2) { // If instance does not had Phone, save is required
        promisePhone2 = _savePhone(newData.Phone2);
      } else { // otherwise just update it
        promisePhone2 = _updatePhone(Address.dataValues.phoneId2, newData.Phone2);
      }
      promisesForAddress.push(promisePhone2);
    }

    // Build and save Fax
    if (newData.Fax) {
      let promiseFax = {};

      if (!Address.dataValues.faxId) { // If instance does not had Phone, save is required
        promiseFax = _savePhone(newData.Fax);
      } else { // otherwise just update it
        promiseFax = _updatePhone(Address.dataValues.faxId, newData.Fax);
      }
      promisesForAddress.push(promiseFax);
    }

    // Resolve al references builds
    return promisesForAddress;
  }

  /**
   * Builds and updates Phone Instance
   * @param newData
   * @returns {Promise.<this|Errors.ValidationError>}
   * @private
   */
  function _updatePhone(phoneId, newData) {
    let deferred = q.defer();

    // Check if provided Phone exists before updating it
    Phone.findOne({
      where: { id: phoneId}
    })
    .then(phone => {
        // If does not exists, reject it
        if (!phone) {
          deferred.reject('Phone ID ' + phoneId + ' was not found.');

        } else { // If exists, update it:
          phone.update(newData)
          .then(phone => {
              deferred.resolve(phone);
            })
          .catch(err => { // Catch error when updating Phone
              deferred.reject(err);
            });
        }
      })

      // Catch error when finding Phone
    .catch(err => {
        deferred.reject(err);
      });

    return deferred.promise;
  }

  /**
   * Builds and deletes Phone Instance
   * @param phoneId
   * @returns {deferred.promise|{then, always}}
   * @private
   */
  function _deletePhone(phoneId) {
    let deferred = q.defer();

    if (!phoneId) {
      deferred.resolve(0);
      return deferred.promise;
    }

    // Check if provided Phone exists before updating it
    Phone.findOne({
      where: { id: phoneId}
    })
    .then(phone => {
        // If does not exists, it's OK too
        if (!phone) {
          deferred.resolve(0);

        } else { // If exists, delete it:
          phone.destroy()
          .then(() => {
              deferred.resolve(0);
            })
          .catch(err => { // Catch error when deleting Phone
              deferred.reject(err);
            });
        }
      })

    // Catch error when finding Phone
    .catch(err => {
        deferred.reject(err);
      });

    return deferred.promise;
  }

  /**
   * Validate if Address belongs to current User
   * @param userId
   * @returns {deferred.promise|{then, always}}
   * @private
   */
  function _validateUser(addressId, userId) {
    let deferred = q.defer();
    deferred.resolve(true); // @TODO: REMOVE ME WHEN CLIENT MODEL IS ADDED!
    //Client.findOne({
    //  where: {userId: userId},
    //  attributes: ['id'],
    //  include: [{
    //    all: true,
    //    nested: true,
    //    attributes: ['id']
    //  }]
    //})
    //.then(client => {
    //    // Check if no User was found:
    //    if (!client) { deferred.reject(false); }
    //
    //    // If Role is Client or Guest:
    //    if(client.User.Role.id > 5) {
    //      // Walk through all Client's Addresses:
    //      client.Addresses.forEach((item) => {
    //        // If Address ID matches with the requested one:
    //        if (item.id === addressId) {
    //          // Resolve true:
    //          return deferred.resolve(true);
    //        }
    //      });
    //      // Resolve false if did not match:
    //      deferred.reject(false);
    //
    //    } else {
    //      deferred.resolve(true); // otherwise, OK!
    //    }
    //
    //  })
    //.catch(err => {
    //    deferred.reject(err);
    //  });
    return deferred.promise;
  }

  exports.getAll = getAddressesByUserId;
  exports.getById = getAddressById;
  exports.create = createAddress;
  exports.edit = editAddress;
  exports.delete = deleteAddress;

})();