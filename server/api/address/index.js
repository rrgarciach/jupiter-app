(function () {
  'use strict';

  var express = require('express');
  var controller = require('./address.controller');
  var router = express.Router();
  var RBAC = require('../../components/RBAC');

  router.get('/me', RBAC.canRetrieveMyAddress(), controller.indexMe);
  router.get('/:id(\\d+)', RBAC.canRetrieveAnyAddress(), controller.show);
  router.get('/me/:id', RBAC.canRetrieveMyAddress(), controller.showMe);
  router.post('/', RBAC.canCreateAddress(), controller.create);
  router.post('/me', RBAC.canCreateMyAddress(), controller.createMe);
  router.put('/:id(\\d+)', RBAC.canEditAnyAddress(), controller.update);
  //router.put('/me', RBAC.canEditMyAddress(), controller.updateMe);
  router.delete('/:id(\\d+)', RBAC.canDeleteAnyAddress(), controller.destroy);
  //router.delete('/me/:id', RBAC.canDeleteMyAddress(), controller.destroyMe);

  module.exports = router;
})();