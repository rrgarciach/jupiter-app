(function () {
  'use strict';

  var AddressesServiceSeq = require('./address.service.seq');

  // Get a single Address from current User
  exports.indexMe = function (req, res) {
    AddressesServiceSeq.getAll(req.user)
      .then(address => {
        if (!address) return res.status(404).send('Not Found');
        return res.status(200).json(address);
      }, err => {
        if (err.code === 401) return res.status(401).send('Unauthorized');
        return handleError(res, err);
      });
  };

  // Get a single Address
  exports.show = function (req, res) {
    AddressesServiceSeq.getById(req.params.id)
      .then(address => {
        if (!address) return res.status(404).send('Not Found');
        return res.status(200).json(address);
      }, err => {
        if (err.code === 401) return res.status(401).send('Unauthorized');
        return handleError(res, err);
      });
  };

  // Get a single Address from current User
  exports.showMe = function (req, res) {
    AddressesServiceSeq.getById(req.user.id)
      .then(address => {
        if (!address) return res.status(404).send('Not Found');
        return res.status(200).json(address);
      }, err => {
        if (err.code === 401) return res.status(401).send('Unauthorized');
        return handleError(res, err);
      });
  };

  // Creates a new Address in the DB.
  exports.create = function(req, res) {
    AddressesServiceSeq.create(req.body)
      .then(function(createdAddress) {
        return res.status(200).json(createdAddress);
      }, function(err) {
        if (err.code === 400) return res.status(400).send('Bad Request');
        if (err.code === 401) return res.status(401).send('Unauthorized');
        if (err.code === 404) return res.status(404).send('Not Found');
        if (err.code === 409) return res.status(409).send('Already Exists');
        return handleError(res, err);
      });
  };

  // Creates a new Address in the DB from current user
  exports.createMe = function(req, res) {
    if (req.body) { req.body.userId = req.user.id; }
    AddressesServiceSeq.create(req.body)
      .then(function(createdAddress) {
        return res.status(200).json(createdAddress);
      }, function(err) {
        if (err.code === 400) return res.status(400).send('Bad Request');
        if (err.code === 401) return res.status(401).send('Unauthorized');
        if (err.code === 404) return res.status(404).send('Not Found');
        if (err.code === 409) return res.status(409).send('Already Exists');
        return handleError(res, err);
      });
  };

  // Updates an existing Address in the DB.
  exports.update = function(req, res) {
    AddressesServiceSeq.edit(req.params.id, req.body)
      .then(function(updatedAddress) {
        return res.status(200).json(updatedAddress);
      }, function(err) {
        if (err.code === 400) return res.status(400).send('Bad Request');
        if (err.code === 401) return res.status(401).send('Unauthorized');
        if (err.code === 404) return res.status(404).send('Not Found');
        return handleError(res, err);
      });
  };

  // Deletes a Address from the DB.
  exports.destroy = function(req, res) {
    AddressesServiceSeq.delete(req.params.id)
      .then(function(success) {
        return res.status(204).send('Removed');
      }, function(err) {
        if (err.code === 401) return res.status(401).send('Unauthorized');
        if (err.code === 404) return res.status(404).send('Not Found');
        return handleError(res, err);
      });
  };

  // Handles error responses
  function handleError(res, err) {
    console.log(err);
    var errorResponse = {
      err: err,
      message: 'Internal Error'
    };
    return res.status(500).send(errorResponse);
  }
})();