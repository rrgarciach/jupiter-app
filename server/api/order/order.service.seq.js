(function () {
  'use strict';

  const models = require('../../models');
  const Order = models.Order;
  const User = models.User;
  const Promoter = models.Promoter;
  //const Client = models.Client;
  const Address = models.Address;
  const AddressesServiceSeq = require('../address/address.service.seq');
  const Phone = models.Phone;
  const OrderStatus = models.OrderStatus;
  const OrderDetail = models.OrderDetail;
  const q = require('q');
  const jsonErrorFactory = require('../../components/json_error');

  /**
   * Get all Orders
   * @param query
   * @returns {deferred.promise|{then, always}}
   */
  function getAllOrders(query) {
    let deferred = q.defer();
    let where = _getDatesQuery(query); // get where query object
    Order.findAndCountAll({
      where: where,
      limit: query.limit,
      offset: query.offset,
      order: query.order,
      include: [
        {
          model: OrderStatus,
          attributes: ['id'],
          as: 'OrderStatus'
        },
        //{
        //  model: Client,
        //  attributes: ['id', 'folio'],
        //  as: 'Client',
        //  include: [{
        //    model: User,
        //    attributes: ['firstName', 'lastName']
        //  }]
        //},
        {
          model: Promoter,
          attributes: ['id'],
          as: 'Promoter',
          include: [{
            model: User,
            attributes: ['firstName', 'lastName']
          }]
        },
        {
          model: Address,
          as: 'Address',
          include: [{
            all: true,
            nested: true,
            attributes: ['id', 'number', 'type']
          }]
        },
        {
          model: OrderDetail,
          attributes: ['sku', 'quantity'],
          as: 'Products'
        }
      ]
    })
      .then(function (orders) {
        deferred.resolve(orders);
      }, function (err) {
        deferred.reject(err);
      });
    return deferred.promise;
  }

  /**
   * Get single Order by ID
   * @param orderId
   * @returns {deferred.promise|{then, always}}
   */
  function getOrderById(orderId) {
    let deferred = q.defer();
    Order.findOne({
      where: {id: orderId},
      include: [
        {
          model: OrderStatus,
          attributes: ['name'],
          as: 'OrderStatus'
        },
        //{
        //  model: Client,
        //  attributes: ['id', 'folio'],
        //  as: 'Client',
        //  include: [{
        //    model: User,
        //    attributes: ['firstName', 'lastName']
        //  }]
        //},
        {
          model: Promoter,
          attributes: ['id'],
          as: 'Promoter',
          include: [{
            model: User,
            attributes: ['firstName', 'lastName']
          }]
        },
        {
          model: Address,
          as: 'Address',
          include: [{
            all: true,
            nested: true,
            attributes: ['id', 'number', 'type']
          }]
        },
        {
          model: OrderDetail,
          attributes: ['sku', 'quantity'],
          as: 'Products'
        }
      ]
    })
      .then(function (order) {
        deferred.resolve(order);
      }, function (err) {
        deferred.reject(err);
      });
    return deferred.promise;
  }

  /**
   * Create a new Order
   * @param newOrderData
   * @param User
   * @returns {deferred.promise|{then, always}}
   */
  // @TODO add throw new Error when validations are added to model (as in ProductsService)
  function createOrder(newOrderData, User) {
    let promiseForCreateOrder = [];

    if (newOrderData.id) { delete newOrderData.id; } // Remove Order ID if it was provided (because it shouldn't)

    /* @TODO: Refactor Promoter and Address resolves as a private method */
    // Resolve Promoter ID from current User
    let promisePromoter = _resolvePromoter(User)
      .then(promoterId => {
        newOrderData.promoterId = promoterId;
      });
    promiseForCreateOrder.push(promisePromoter.promise);

    /* @TODO Build and save Client here */

    // Validate Address
    let promiseAddress = q.defer();
    if (newOrderData.addressId) {
      //let promiseAddress = _getAddress(newOrderData.addressId)
      AddressesServiceSeq.getById(newOrderData.addressId, User.id)
        .then(address => {
          if (!address) {
            let jsonError = jsonErrorFactory(404, 'Not Found', 'Provided Address was not found.');
            promiseAddress.reject(jsonError);
          } else {
            newOrderData.Address = address;
            promiseAddress.resolve(address);
          }
        })
        .catch(err => {
          let jsonError = jsonErrorFactory(400, 'Bad Request', err);
          promiseAddress.reject(jsonError);
        });

    } else { // If not Address ID provided, reject request
      let jsonError = jsonErrorFactory(400, 'Bad Request', 'Address ID not provided');
      promiseAddress.reject(jsonError);
    }
    promiseForCreateOrder.push(promiseAddress.promise);

    return q.all(promiseForCreateOrder)
    .then(() => {
        let deferred = q.defer();

        newOrderData.status = 1; // Set default Order status ("Captured")

        let newOrderInstance = Order.build(newOrderData);
        newOrderInstance.save()
          .then(newOrder => {
            deferred.resolve(newOrder);
          })
          .catch(err => {
            if (err.message.indexOf('Validation error') > -1) { // indexOf because message is sent as a string
              let jsonError = jsonErrorFactory(400, 'Bad Request', err.message);
              deferred.reject(jsonError);
              throw new Error('Bad Request'); // got to catch this way because validations are not async =(
            } else {
              deferred.reject(err);
            }
            return deferred.promise;
          });

        return deferred.promise;
      })
      .catch(err => {
        let deferred = q.defer();
        let jsonError = jsonErrorFactory(400, 'Bad Request', err);
        deferred.reject(jsonError);
        return deferred.promise;
      });

  }

  /**
   * Update an Order by ID
   *
   * @param orderId
   * @param newData
   * @returns {deferred.promise|{then, always}}
   */
  // @TODO add throw new Error when validations are added to model (as in ProductsService)
  function editOrder(orderId, newData) {
    let promiseForCreateOrder = [];

    if(newData.id) delete newData.id; // Remove ID to avoid editing it.

    /* @TODO: Refactor Promoter and Address resolves as a private method */
    // Resolve Promoter ID from current User
    let promisePromoter = _resolvePromoter(User)
      .then(promoterId => {
        newData.promoterId = promoterId;
      });
    promiseForCreateOrder.push(promisePromoter.promise);

    /* @TODO Build and save Client here */

    // Validate Address
    let promiseAddress = q.defer();
    if (newData.addressId) {
      //let promiseAddress = _getAddress(newOrderData.addressId)
      AddressesServiceSeq.getById(newData.addressId, User.id)
        .then(address => {
          if (!address) {
            let jsonError = jsonErrorFactory(404, 'Not Found', 'Provided Address was not found.');
            promiseAddress.reject(jsonError);
          } else {
            newData.Address = address;
            promiseAddress.resolve(address);
          }
        })
        .catch(err => {
          let jsonError = jsonErrorFactory(400, 'Bad Request', err);
          promiseAddress.reject(jsonError);
        });

    } else { // If not Address ID provided, reject request
      let jsonError = jsonErrorFactory(400, 'Bad Request', 'Address ID not provided');
      promiseAddress.reject(jsonError);
    }
    promiseForCreateOrder.push(promiseAddress.promise);

    return q.all(promiseForCreateOrder)
      .then(() => {
        var deferred = q.defer();
        Order.findOne({
          where: {id: orderId}
        })
          .then(order => {
            if (!order) {
              let jsonError = jsonErrorFactory(404, 'Not Found', 'Provided Order ID not found');
              deferred.reject(jsonError);

            } else {
              order.update(newData)
                .then(updatedOrder => {
                  deferred.resolve(updatedOrder);
                })
                .catch(err => {
                  // Catch validation Error throws:
                  if (err.message.indexOf('Validation error') > -1) { // indexOf because message is sent as a string
                    let jsonError = jsonErrorFactory(400, 'Not Found', err.message);
                    deferred.reject(jsonError);
                    throw new Error('Bad Request'); // got to catch this way because validations are not async =(
                  } else {
                    deferred.reject(err);
                  }
                  return deferred.promise;
                });
            }
          })
          .catch(err => {
            return deferred.reject(err);
          });
        return deferred.promise;
      })
      .catch(err => {
        let deferred = q.defer();
        let jsonError = jsonErrorFactory(400, 'Bad Request', err);
        deferred.reject(jsonError);
        return deferred.promise;
      });
  }

  /**
   * Changes the status of the Order
   * @param orderId
   * @param newStatus
   * @returns {deferred.promise|{then, always}}
   */
  function changeOrderStatus(orderId, newStatus) {
    let deferred = q.defer();

    Order.findOne({
      where: {id: orderId},
      attributes: ['id']
    })
      .then(order => {
        if (!order) {
          let jsonError = jsonErrorFactory(404, 'No Found', 'Provided Order ID not found');
          deferred.reject(jsonError);

        } else {
          order.setDataValue('status', newStatus); // Set new Status

          order.update({status: newStatus})
            .then(function (updatedOrder) {
              deferred.resolve(updatedOrder);
            })
            .catch(err => {
              deferred.reject(err);
              return deferred.promise;
            });
        }
      })
      .catch(err => {
        return deferred.reject(err);
      });
      return deferred.promise;
  }

  /**
   * Delete and Order by ID
   * @param orderId
   * @returns {deferred.promise|{then, always}}
   */
  function deleteOrder(orderId) {
    var deferred = q.defer();

    Order.findOne({
      where: {id: orderId},
      attributes: ['id']
    })
      .then(order => {
        if (!order) {
          let jsonError = jsonErrorFactory(404, 'No Found', 'Provided Order ID not found');
          deferred.reject(jsonError);

        } else {
          order.destroy()
            .then(function () {
              deferred.resolve(true);
            }, function (err) {
              deferred.reject(err);
              return deferred.promise;
            });
        }
      })
      .catch(err => {
        deferred.reject(err);
      });
    return deferred.promise;
  }

  /**
   * Resolves Promoter from current User
   * @param User
   * @returns {deferred.promise|{then, always}}
   * @private
   */
  function _resolvePromoter(User) {
    // If User has Client role
    if (User.Role && User.Role.name === 'Client') { // If User is not a Promoter, then it has to be the Client itself who captured Order
      return _getPromoterFromClientById(User.id)

    } else if (User.Role && User.Role.name !== 'Client') {
      // The User that captured the Order is suppose to be a Promoter
      return _getPromoterById(User.id)

    } else { // This is in case there's no Role attached
      let deferred = q.defer();
      let jsonError = jsonErrorFactory(400, 'Bad Request', "Current Order's Promoter has no Role attached");
      deferred.reject(jsonError);
      return deferred.promise;
    }
  }

  /**
   * Get Promoter from given User ID
   * @param userId
   * @returns {deferred.promise|{then, always}}
   * @private
   */
  function _getPromoterById(userId) {
    let deferred = q.defer();

    Promoter.findOne({ where: {id: userId}, attributes: ['id'] })
      .then(function (promoter) {
        if (!promoter) {
          deferred.resolve(1);
        } else {
          deferred.resolve(promoter.id);
        }
      })
      .catch(function (err) {
        deferred.reject(err);
      });

    return deferred.promise;
  }

  /**
   * Get Client's Promoter from given User ID
   * @param userId
   * @returns {deferred.promise|{then, always}}
   * @private
   */
  function _getPromoterFromClientById(userId) {
    let deferred = q.defer();

    //Client.findOne({where: {id: userId}, attributes: ['promoterId']})
    //  .then(function (client) {
    //    if (!client) {
          deferred.reject(1);
    //    } else {
    //      deferred.resolve(client.promoterId);
    //    }
    //  })
    //  .catch(function (err) {
    //    deferred.reject(err);
    //  });

    return deferred.promise;
  }

  /**
   * Get Address referenced in Order
   * @param addressId
   * @returns {deferred.promise|{then, always}}
   * @private
   */
  function _getAddress(addressId) {
    let deferred = q.defer();
    Address.findOne({ where: {id: addressId}, attributes: ['id'] })
      .then(function (address) {
        if (!address) {
          let jsonError = jsonErrorFactory(400, 'Bad Request', 'Provided Address not found');
          deferred.reject(jsonError);
        } else {
          deferred.resolve(address);
        }
      })
      .catch(function (err) {
        let jsonError = jsonErrorFactory(500, 'Internal Error', err);
        deferred.reject(jsonError);
      });
    return deferred.promise;
  }

  /**
   * Builds and saves Address Instance
   * @param newAddressData
   * @returns {deferred.promise|{then, always}}
   * @private
   */
  function _saveAddress(newAddressData) {
    let promisesForAddress = []; // Array of promises

    // Build and save Phone 1
    if (newAddressData.Phone1) {
      let promisePhone1 = _savePhone(newAddressData.Phone1)
        .then(phone1 => {
          newAddressData.phoneId1 = phone1.id;
        });
      promisesForAddress.push(promisePhone1);
    } else {
      let deferred = q.defer();
      let jsonError = jsonErrorFactory(400, 'Bad Request', 'Phone 1 data was not provided');
      deferred.reject(jsonError);
      return deferred.promise;
    }

    // Build and save Phone 2
    if (newAddressData.Phone2) {
      let promisePhone2 = _savePhone(newAddressData.Phone2)
        .then(phone2 => {
          newAddressData.phoneId2 = phone2.id;
        });
      promisesForAddress.push(promisePhone2);
    }

    // Build and save Fax
    if (newAddressData.Fax) {
      let promiseFax = _savePhone(newAddressData.Fax)
        .then(fax => {
          newAddressData.faxId = fax.id;
        });
      promisesForAddress.push(promiseFax);
    }

    // Resolve al references builds
    return q.all(promisesForAddress)
    .then(() => {
        let newAddressInstance = Address.build(newAddressData);
        return newAddressInstance.save()
      });

  }

  /**
   * Builds and saves Phone Instance
   * @param newPhoneData
   * @returns {Promise.<this|Errors.ValidationError>}
   * @private
   */
  function _savePhone(newPhoneData) {
    let newPhoneInstance = Phone.build(newPhoneData);
    return newPhoneInstance.save()
  }

  /**
   * Prepares "where" object to be injected in searches
   * @param query The request query (GET)
   * @returns {{}} The where object to inject to sequelize request method
   * @private
   */
  function _getDatesQuery(query) {
    let where = {}; // "where" query object

    // If the target field has been set (ID is default):
    if (query.like) { // if it's a "like" search
      if (query.client) {
        where.client = {$like :query.client + '%'};
      } else if (query.promoter) {
        where.promoterId = {$like :query.promoter + '%'};
      }
    } else { // if it's an "=" search
      if (query.client) {
        where.client = query.client;
      } else if (query.promoter) {
        where.promoterId = query.promoter;
      } else if (query.status) {
        where.status = query.status;
      }
    }

    // If a specific date has been set:
    if (query.date) {
      where.date = query.date;
    } else if (query.from && query.to) {
      where.date = {
        $gte: query.from,
        $lte: query.to
      };
    }

    return where;
  }

  exports.getAll = getAllOrders;
  exports.getById = getOrderById;
  exports.create = createOrder;
  exports.edit = editOrder;
  exports.changeStatus = changeOrderStatus;
  exports.delete = deleteOrder;

})();