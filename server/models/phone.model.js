(function () {
  'use strict';

  var Sequelize = require('sequelize');

  module.exports = function(sequelize) {
    var Phone = sequelize.define('Phone', {
      id: {
        type: Sequelize.INTEGER,
        unique: true,
        primaryKey: true,
        autoIncrement: true,
        validate: {
          isInt: true
        }
      },
      number: {
        type: Sequelize.STRING,
        allowNull: false
      },
      type: {
        type: Sequelize.ENUM,
        values: ['Phone', 'Fax', 'Mobile']
      }
    }, {
      timestamps: false,
      paranoid: false
    });

    return Phone;
  }

})();