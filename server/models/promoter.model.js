(function () {
  'use strict';

  var Sequelize = require('sequelize');

  module.exports = function(sequelize) {
    var Promoter = sequelize.define('Promoter', {
        id: {
          type: Sequelize.INTEGER,
          unique: true,
          primaryKey: true,
          autoIncrement: true,
          //validate: {
          //  isInt: true
          //}
        },
        userId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          validate: {
            isInt: true
          },
          references: {
            model: 'Users',
            key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE'
        },
        addressId: {
          type: Sequelize.STRING,
          allowNull: false,
          validate: {
            isInt: true
          }
        }
      },
      {
        timestamps: false,
        paranoid: false,
        classMethods: {
          associate: function (models) {
            // associations can be defined here
            Promoter.belongsTo(models.User, {foreignKey: 'userId'});
            Promoter.belongsTo(models.Address, {foreignKey: 'addressId'});
            //Promoter.belongsTo(models.Order, {foreignKey: 'PromoterId'});
          }
        }
      });

    return Promoter;
  }

})();