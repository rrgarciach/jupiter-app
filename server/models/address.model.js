(function () {
  'use strict';

  var Sequelize = require('sequelize');

  module.exports = function(sequelize) {
    var Address = sequelize.define('Address', {
      id: {
        type: Sequelize.INTEGER,
        unique: true,
        primaryKey: true,
        autoIncrement: true,
        validate: {
          isInt: true
        }
      },
      street: {
        type: Sequelize.STRING,
        allowNull: false
      },
      number: {
        type: Sequelize.INTEGER,
        validate: {
          isInt: true
        }
      },
      interior: {
        type: Sequelize.STRING
      },
      suburb: {
        type: Sequelize.STRING,
        allowNull: false
      },
      zip: {
        type: Sequelize.INTEGER,
        validate: {
          isInt: true
        }
      },
      city: {
        type: Sequelize.STRING,
        allowNull: false
      },
      county: {
        type: Sequelize.STRING,
        allowNull: false
      },
      state: {
        type: Sequelize.STRING,
        allowNull: false
      },
      phoneId1: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Phones',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      phoneId2: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Phones',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      faxId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Phones',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      latitude: {
        type: Sequelize.FLOAT,
        allowNull: false
      },
      longitude: {
        type: Sequelize.FLOAT,
        allowNull: false
      }
    }, {
      timestamps: false,
      paranoid: false,
      classMethods: {
        associate: function (models) {
          // associations can be defined here
          Address.belongsTo(models.Phone, {as: 'Phone1', foreignKey: 'phoneId1'});
          Address.belongsTo(models.Phone, {as: 'Phone2', foreignKey: 'phoneId2'});
          Address.belongsTo(models.Phone, {as: 'Fax', foreignKey: 'faxId'});
        }
      }
    });

    return Address;
  }

})();