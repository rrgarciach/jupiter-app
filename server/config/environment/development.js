(function () {
  'use strict';
  var dbConfig = require('../db').development;

  // Development specific configuration
  // ==================================
  module.exports = {
    // MongoDB connection options
    mongo: {
      uri: 'mongodb://localhost/jupiter-dev'
    },
    seedDB: false,

    db: dbConfig,
    debug: true,
    //mailConfig: {
    //  transport: {
    //    host: "mailtrap.io",
    //    port: 465,
    //    debug: true,
    //    auth: {
    //      user: "4192126a7a511a86b",
    //      pass: "5949e3fadb762a"
    //    }
    //  },
    //  supportEmail: 'soporte@distribuidoragc.com'
    //},
    host: 'localhost:9090'
  };

})();
