(function () {
  "use strict";

  const config = require('../../config/environment');

  module.exports = function (code, message, debugMessage) {
    let errorResponse = {};
    errorResponse.message = message;
    errorResponse.code = code;
    if (debugMessage && config.env === 'development') {
      errorResponse.debugMessage = debugMessage;
    }
    return errorResponse;
  }

})();